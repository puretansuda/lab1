/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryMethod;

/**
 *
 * @author Room107
 */
public class DBConnectionFactory {
    public DBConnetion createDBConnection(String brand, String host, String port, String dbName, String user, String password){
        if(brand.equalsIgnoreCase("mysql")){
            return new MySQLDBConnection(host, port, dbName, user, password);
        }else{
            return null;
        }
    }
}
