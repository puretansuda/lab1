/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryMethod;

import java.sql.ResultSet;

/**
 *
 * @author Room107
 */
public interface DBConnetion {
    ResultSet execouteQuery(String query);
    void update(String command);
    void close();
}
